#!/usr/bin/env python 

from std_msgs.msg import Float32
from ppg.srv import *
import rospy

def handle_do_filter_calc(req):
        
        input = []
        filtered = []
        input = req.input
        filtered = req.filtered

        if len(input) < 4:
            response = []
            response = req.input
            return DoFilterCalcResponse(response[len(response)-1])
        else:
            y = -0.05757*filtered[len(filtered)-1]-0.001867*filtered[len(filtered)-2]
            s = 0.9898*input[len(input)-1]+0.04553*input[len(input)-2]
            return DoFilterCalcResponse(float(y+s))

def do_filter_calc_server():
        rospy.init_node('do_filter_calc_server')
        s = rospy.Service('do_filter_calc',DoFilterCalc, handle_do_filter_calc)
        rospy.spin()

if __name__ == "__main__":
        do_filter_calc_server()
