#!/usr/bin/env python
import sys
import  rospy
from std_msgs.msg import Float32
from ppg.srv import DoFilterCalc
from ppg.msg import Readingmsg

global pub
global inputArray
global filteredArray

def callback(data):
    rospy.wait_for_service('do_filter_calc')
    try:
        do_filter_calc = rospy.ServiceProxy('do_filter_calc', DoFilterCalc)
        msg = Readingmsg()
        msg.timestamp = data.timestamp
        msg.reading = float(data.reading)
        inputArray.append(data.reading)
        
        filterSrv = do_filter_calc(inputArray, filteredArray)
        filteredArray.append(filterSrv.filteredInput)
        
        msg.filteredReading = float(filteredArray[len(filteredArray)-1])
        pub.publish(msg)

    except rospy.ServiceException, e:
        print "Service call failed %s" %e

def listener():
    rospy.init_node('ip_filter', anonymous=True)
    rospy.Subscriber("/fpmg_raw", Readingmsg, callback)
    rospy.spin()

if __name__ == '__main__':
    try:
        inputArray = []
        filteredArray = []
        pub = rospy.Publisher('/fpmg_filtered', Readingmsg, queue_size=10)
        listener()
    except rospy.ROSInterruptException, e:
        pass
