#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Float32
from std_msgs.msg import Time
import Adafruit_BBIO.ADC as ADC
import rosbag
import sys
from ppg.msg import Readingmsg
import pyrosbag as prb

sensor_pin = 'P9_40'
ADC.setup()

def fotoplesmytograf(myArg):
        pub = rospy.Publisher('/fpmg_raw', Readingmsg, queue_size=10)
        rospy.init_node('fotoplesmytograf', anonymous=True)
        rate = rospy.Rate(100) #100hz
        
        time = rospy.get_rostime()
        start = time
        while not rospy.is_shutdown():
            if myArg == "0":
                try:
                    bagW = rosbag.Bag('reading.bag', 'w')
                    time =  rospy.get_rostime()
                    reading = Float32()
                    reading.data = ADC.read(sensor_pin)
                    msg = Readingmsg()
                    msg.reading = reading.data
                    msg.timestamp = (time - start)
                    bagW.write('Readingmsg', reading, msg.timestamp)
                    pub.publish(msg)
                    rospy.loginfo(time-start)
                    rate.sleep()
                finally:
                    bagW.close()
            else:
                with prb.BagPlayer("reading.bag") as read:
                    read.play()
                    while read.is_running:
                        inpinputs = input()
                        

if __name__ == '__main__' :
    if len(sys.argv) < 2:
            print("usage: fotoplesmytograf.py arg1. 0:For live data, 1:For rosbag data")
    else:
        try:
                fotoplesmytograf(sys.argv[1])
        except rospy.ROSInterruptException:
                pass
