#! /usr/bin/env python
import rospy
from std_msgs.msg import Float32
from ppg.msg import Readingmsg

def callback(data):
    
    rospy.loginfo("Reading: %s Filtered: %s Time: %s" % (data.reading,data.filteredReading, data.timestamp))

def listener():
        rospy.init_node('evaluate_data', anonymous=True)
        rospy.Subscriber('/fpmg_filtered', Readingmsg, callback)
        rospy.spin()

if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        pass
